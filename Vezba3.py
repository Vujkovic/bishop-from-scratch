#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 25 18:43:49 2022

@author: milan
"""

import random
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import math
#%%prvi_zadatak
def density(x,mu,variance,apo):
    p = [0 for i in range(len(x))]
    for i in range(len(mu)):
        p+=(1/np.sqrt(2*np.pi*variance[i]))*np.exp(-0.5*((x-mu[i])/np.sqrt(variance[i]))**2)*apo[i]
    return p

def sampling(n,line,mu,variance,apo):
    pdf = density(line,mu,variance,apo)
    return np.random.choice(line,n,p=pdf/sum(pdf))

def euclidean_distance(x1, x2):
    return np.sqrt(np.sum((x1-x2)**2))

def knn_estimate(space, sample, k, c):
    
    N = len(sample) 
    pdf = []
    
    for x in space:
        est=0
        distances = np.array([euclidean_distance(s, x) for s in sample])
        sort_idxs = np.argsort(distances)
        R = distances[sort_idxs]
        if R[k] == 0:
            q = k+1
            while(R[q]==0):
                q +=1
            est=k/(N*c*R[q+k])
        else:
            est = k / (N*c*R[k])
        pdf.append(est)

    return pdf
    

#Variables a):
mu1 = [0,2.5]
variance1 = [0.2,0.2]
apo1 = [1/3,2/3]
sigma1 = []
for i in variance1:
    sigma1.append(math.sqrt(i))

line1 = np.linspace(-2,5, 10000)

#Variables b):
mu2 = [-1,2,4,5,7]
variance2 = [0.2,0.4,0.4,0.1,0.3]
apo2= [0.125,0.25,0.25,0.25,0.125]
sigma2 = []
for i in variance2:
    sigma2.append(math.sqrt(i))

line2 = np.linspace(-3,10, 10000)

#drugi_zadatak preveliko k je previse neosetljivo na gustinu verovatanoce, a premalo je previse osetljivo pa ima velike raspone
k = 20
c = 2

for i in [100,500,1000,5000,10000]:
#a)
    pdf1 = density(line1, mu1, variance1, apo1)
    plt.plot(line1, pdf1, color = 'green')
    sample1 = sampling(i, line1, mu1, variance1, apo1)
    unique,counts = np.unique(sample1, return_counts=True)
    est1 = knn_estimate(line1, sample1, k, c)
    plt.plot(line1,est1, color='red')
    plt.show()

#b)
    pdf2 = density(line2, mu2, variance2, apo2)
    plt.plot(line2, pdf2, color = 'green')
    sample2 = sampling(i, line2, mu2, variance2, apo2)
    unique,counts = np.unique(sample2, return_counts=True)
    est2 = knn_estimate(line2, sample2, k, c)
    plt.plot(line2,est2, color='red')
    plt.show()

#%%

#%%treci_zadatak
mu = [-4,3] 
variance = [2, 1.5]
sigma = []
for i in variance:
    sigma.append(math.sqrt(i))

line = np.linspace(-10,10,1000)

plt.plot(line, stats.norm.pdf(line, mu[0], sigma[0]), color='blue')
plt.plot(line, stats.norm.pdf(line, mu[1], sigma[1]), color='blue')


def density(x,mu,variance):
    p=(1/np.sqrt(2*np.pi*variance))*np.exp(-0.5*((x-mu)/np.sqrt(variance))**2)
    return p

def sampling(n,mu,variance):
    pdf = density(line,mu,variance)
    return np.random.choice(line,n,p=pdf/sum(pdf))


sample1 = sampling(1000,mu[0],variance[0])
sample2 = sampling(1000,mu[1],variance[1])
k=42
est1 = knn_estimate(line, sample1, k, c)
plt.plot(line,est1, color='red')

est2 = knn_estimate(line, sample2, k, c)
plt.plot(line,est2, color='red')
plt.show()

def MLRTest(prob1,prob2,idx):
    prob1 = prob1[idx]
    prob2 = prob2[idx]
    if (prob1/prob2) > 1:
        return 0;
    else:
        return 1;
    
zeros = np.array([0 for i in range(1000)]) 
sample1 = np.column_stack((sample1,zeros))

ones = np.array([1 for i in range(1000)]) 
sample2 = np.column_stack((sample2,ones))
sample = np.append(sample1,sample2,axis=0)

classify = []
for point in sample:
    classify.append(MLRTest(est1,est2,line.tolist().index(point[0])))

s = 0 
for i in range(len(classify)):
    if sample[i][1] == classify[i]:
        s+=1
print(s/len(classify))
#%%

#%%cetvrti_zadatak
k = 15
def KNNclass(k,element,sample):
    prva = 0
    druga = 0
    distances = np.array([euclidean_distance(s, element) for s in sample[:,0]])
    sort_idxs = np.argsort(distances)
    R = sample[sort_idxs]
    for i in range(k):
        if R[i][1] == 0:
            prva +=1
        else:
            druga +=1
        i+=1

    if prva > druga:
        return 0
    else:
        return 1

classify = []
for point in sample:
    classify.append(KNNclass(k,point[0],sample))
    
s = 0 
for i in range(len(classify)):
    if sample[i][1] == classify[i]:
        s+=1
print(s/len(classify))
#%%