import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
import math

def density(x,mu,variance,apo):
    p = [0 for i in range(len(x))]
    for i in range(len(mu)):
        p+=(1/np.sqrt(2*np.pi*variance[i]))*np.exp(-0.5*((x-mu[i])/np.sqrt(variance[i]))**2)*apo[i]
    return p

def sampling(n,par,weights):
    mixture_idx = np.random.choice(len(weights), size=n, p=weights)
    s = np.fromiter((stats.norm.rvs(*(par[i])) for i in mixture_idx),dtype=np.float64)
    return s

def euclidean_distance(x1, x2):
    return np.sqrt(np.sum((x1-x2)**2))

def knn_estimate(space, sample, k, c):
    N = len(sample) 
    e = []
    for x in space:
        distances = np.array([euclidean_distance(x, s) for s in sample])
        sort_idxs = np.argsort(distances)
        R = distances[sort_idxs]
        est = k / (N*c*R[k-1])
        e.append(est)
    return e

def knn_classify(k,element,sample):
    distances = np.array([euclidean_distance(s, element) for s in sample[:,0]])
    k_idxs = np.argsort(distances)[:k]
    k_nearest_classes = [sample[i,1] for i in k_idxs]
    most_common = stats.mode(k_nearest_classes)
    return most_common[0][0]

def chosen_one(s):
    left=[]
    right=[]
    for x in line:
        if x==s:
            return x
        elif x<s:
            left.append(x)
        else:
            right.append(x)          
    left_distance = euclidean_distance(left[-1], s)
    right_distance = euclidean_distance(right[0], s)    
    if left_distance < right_distance:
        return left[-1]
    else:
        return right[0]
    
def MLRTest(prob1,prob2,idx):
    prob1 = prob1[idx]
    prob2 = prob2[idx]
    if (prob1/prob2) > 1:
        return 0;
    else:
        return 1;
    
#%%Zadatak 1:
    
#Variables a):
mu1 = [0,2.5]
variance1 = [0.2,0.2]
apo1 = [1/3,2/3]
sigma1 = []
for i in variance1:
    sigma1.append(math.sqrt(i))

line1 = np.linspace(-2,5, 10000)


#Variables b):
mu2 = [-1,2,4,5,7]
variance2 = [0.2,0.4,0.4,0.1,0.3]
apo2= [0.125,0.25,0.25,0.25,0.125]
sigma2 = []
for i in variance2:
    sigma2.append(math.sqrt(i))

line2 = np.linspace(-3,10, 10000)

k=20
c=2

for N in [100,500,1000,5000,10000]:
#a)
    pdf1 = density(line1, mu1, variance1, apo1)
    plt.plot(line1, pdf1, color = 'green')
    
    par1=[]
    for i in range(len(apo1)):
        par1.append([mu1[i],sigma1[i]])
    
    sample1 = sampling(N, np.array(par1), np.array(apo1))
    
    est1 = knn_estimate(line1, sample1, k, c)
    plt.plot(line1,est1, color='red', alpha=0.7)
    plt.show()

#b)
    pdf2 = density(line2, mu2, variance2, apo2)
    plt.plot(line2, pdf2, color = 'green')
    
    par2=[]
    for i in range(len(apo2)):
        par2.append([mu2[i],sigma2[i]])

    sample2 = sampling(N, np.array(par2), np.array(apo2))

    est2 = knn_estimate(line2, sample2, k, c)
    plt.plot(line2,est2, color='red', alpha=0.7)
    plt.show()


#%%Zadatak 3:
mu = [-4,3] 
variance = [2, 1.5]
sigma = []
for i in variance:
    sigma.append(math.sqrt(i))

line = np.linspace(-10,10,1000)

plt.plot(line, stats.norm.pdf(line, mu[0], sigma[0]), color='green')
plt.plot(line, stats.norm.pdf(line, mu[1], sigma[1]), color='green')

k=20
c=2
N=1000
    
sample1 = stats.norm.rvs(mu[0],sigma[0],N)
sample2 = stats.norm.rvs(mu[1],sigma[1],N)

zeros = np.array([0 for i in range(N)]) 
sample1 = np.column_stack((sample1,zeros))

ones = np.array([1 for i in range(N)]) 
sample2 = np.column_stack((sample2,ones))
sample = np.append(sample1,sample2,axis=0)
    
est1 = knn_estimate(line, sample1, k, c)
est2 = knn_estimate(line, sample2, k, c)

classify1 = []
for s in sample[:,0]:
    classify1.append(MLRTest(est1,est2,line.tolist().index(chosen_one(s))))

correct1 = 0 
for i in range(len(classify1)):
    if sample[i][1] == classify1[i]:
        correct1+=1
print("MRLTest misclassified: ", ((N*2-correct1)/(N*2))*100,"%")

#%%Zadatak 4:

classify2 = []
for point in sample:
    classify2.append(knn_classify(k,point[0],sample))
    
correct2 = 0 
for i in range(len(classify2)):
    if sample[i][1] == classify2[i]:
        correct2+=1
print("KNN misclassified: ", ((N*2-correct2)/(N*2))*100,"%")

#%%
    

    
    

