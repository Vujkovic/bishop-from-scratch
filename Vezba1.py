#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 22:38:12 2022

@author: milan
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import math


#prvi zadataka
#%% variance_only
#1.a
mu = [0,3] 
variance = [1.5, 4]
sigma = []
for i in variance:
    sigma.append(math.sqrt(i))

x = []    
for m,s in mu,sigma:
    x.append(np.linspace(m - 4*s, m + 4*s, 100))

plt.plot(x[0], stats.norm.pdf(x[0], mu[0], sigma[0]), color='blue')
plt.plot(x[1], stats.norm.pdf(x[1], mu[1], sigma[1]), color='red')
plt.show()

#1.b
mu = [-4,3] 
variance = [2, 1.5]
sigma = []
for i in variance:
    sigma.append(math.sqrt(i))

x = []    
for m,s in mu,sigma:
    x.append(np.linspace(m - 5*s, m + 5*s, 100))


plt.plot(x[0], stats.norm.pdf(x[0], mu[0], sigma[0]), color='blue')
plt.plot(x[1], stats.norm.pdf(x[1], mu[1], sigma[1]), color='red')
plt.show()

#1.c
mu = [2,2] 
variance = [2, 1]
sigma = []
for i in variance:
    sigma.append(math.sqrt(i))

x = []    
for m,s in mu,sigma:
    x.append(np.linspace(m - 4*s, m + 4*s, 100))


plt.plot(x[0], stats.norm.pdf(x[0], mu[0], sigma[0]), color='blue')
plt.plot(x[1], stats.norm.pdf(x[1], mu[1], sigma[1]), color='red')
plt.show()

#%% covariance_matrix

#1.d
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

import numpy as np
from scipy.stats import multivariate_normal

#
x, y = np.mgrid[-5.0:20.0:300j, -5.0:15.0:300j]

# Need an (N, 2) array of (x, y) pairs.
xy = np.column_stack([x.flat, y.flat])

mu = [np.array([3.0, 2.0]),np.array([8.0, 5.0])]

covariance = [[[1.5,0],[0, 1.5]],[[3,0],[0,3]]]

z =[]
for i in range(len(mu)):
    zp = multivariate_normal.pdf(xy, mean=mu[i], cov=covariance[i])
    zp = zp.reshape(x.shape)
    z.append(zp)
    

fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')

for i in range(len(mu)):
    ax.plot_surface(x,y,z[i], cmap = cm.turbo, antialiased=True)
    #ax.plot_wireframe(x,y,z[i])

plt.show()

#1.e
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

import numpy as np
from scipy.stats import multivariate_normal

x, y = np.mgrid[-5.0:20.0:300j, -5.0:15.0:300j]

# Need an (N, 2) array of (x, y) pairs.
xy = np.column_stack([x.flat, y.flat])

mu = [np.array([3.0, 2.0]),np.array([8.0, 5.0])]

covariance = [[[1,0],[0, 3]],[[2,0],[0,1]]]

z =[]
for i in range(len(mu)):
    zp = multivariate_normal.pdf(xy, mean=mu[i], cov=covariance[i])
    zp = zp.reshape(x.shape)
    z.append(zp)
    

fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')

for i in range(len(mu)):
    ax.plot_surface(x,y,z[i], cmap = cm.turbo, antialiased=True)
    #ax.plot_wireframe(x,y,z[i])

plt.show()

#1.f
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

import numpy as np
from scipy.stats import multivariate_normal

x, y = np.mgrid[-5.0:20.0:300j, -5.0:15.0:300j]

# Need an (N, 2) array of (x, y) pairs.
xy = np.column_stack([x.flat, y.flat])

mu = [np.array([3.0, 2.0]),np.array([8.0, 5.0])]

covariance = [[[1,0.6],[0.6 , 3]],[[2,-0.6],[-0.6,1]]]

z =[]
for i in range(len(mu)):
    zp = multivariate_normal.pdf(xy, mean=mu[i], cov=covariance[i])
    zp = zp.reshape(x.shape)
    z.append(zp)
    

fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')

for i in range(len(mu)):
    ax.plot_surface(x,y,z[i], cmap = cm.turbo, antialiased=True)
    #ax.plot_wireframe(x,y,z[i])

plt.show()
#%%

#%%drugi zadatak
#1)
import matplotlib.pyplot as plt
from matplotlib import cm
#from mpl_toolkits.mplot3d import Axes3D

import numpy as np
from scipy.stats import multivariate_normal

mu = [0,0]
cov = [[2,0],[0,2]]

points=np.random.default_rng().multivariate_normal(mu,cov, size = 600)

# Need an (N, 2) array of (x, y) pairs.
#xy = np.column_stack([x.flat, y.flat])


z =[]
zp = multivariate_normal.pdf(points, mu, cov=cov)
#zp = zp.reshape(x.shape)
z.append(zp)
    

fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')

ax.plot_trisurf(points[:,0],points[:,1],z[0], cmap = cm.turbo, antialiased=True)

plt.show()

#2)
mu = [0,0]
cov = [[0.1,0],[0,0.1]]

points=np.random.default_rng().multivariate_normal(mu,cov, size = 600)

# Need an (N, 2) array of (x, y) pairs.
#xy = np.column_stack([x.flat, y.flat])


z =[]
zp = multivariate_normal.pdf(points, mu, cov=cov)
#zp = zp.reshape(x.shape)
z.append(zp)
    

fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')

ax.plot_trisurf(points[:,0],points[:,1],z[0], cmap = cm.turbo, antialiased=True)

plt.show()

#3)
mu = [0,0]
cov = [[0.2,0],[0,2]]

points=np.random.default_rng().multivariate_normal(mu,cov, size = 600)

# Need an (N, 2) array of (x, y) pairs.
#xy = np.column_stack([x.flat, y.flat])


z =[]
zp = multivariate_normal.pdf(points, mu, cov=cov)
#zp = zp.reshape(x.shape)
z.append(zp)
    

fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')

ax.plot_trisurf(points[:,0],points[:,1],z[0], cmap = cm.turbo, antialiased=True)

plt.show()

#4)
mu = [0,0]
cov = [[2,0],[0,0.2]]

points=np.random.default_rng().multivariate_normal(mu,cov, size = 600)

# Need an (N, 2) array of (x, y) pairs.
#xy = np.column_stack([x.flat, y.flat])


z =[]
zp = multivariate_normal.pdf(points, mu, cov=cov)
#zp = zp.reshape(x.shape)
z.append(zp)
    

fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')

ax.plot_trisurf(points[:,0],points[:,1],z[0], cmap = cm.turbo, antialiased=True)

plt.show()

#5)
mu = [0,0]
cov = [[1,-0.7],[-0.7,2.5]]

points=np.random.default_rng().multivariate_normal(mu,cov, size = 600)

# Need an (N, 2) array of (x, y) pairs.
#xy = np.column_stack([x.flat, y.flat])


z =[]
zp = multivariate_normal.pdf(points, mu, cov=cov)
#zp = zp.reshape(x.shape)
z.append(zp)
    

fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')

ax.plot_trisurf(points[:,0],points[:,1],z[0], cmap = cm.turbo, antialiased=True)

plt.show()

#6)
mu = [0,0]
cov = [[2.5,0.7],[0.7,1]]

points=np.random.default_rng().multivariate_normal(mu,cov, size = 600)

# Need an (N, 2) array of (x, y) pairs.
#xy = np.column_stack([x.flat, y.flat])


z =[]
zp = multivariate_normal.pdf(points, mu, cov=cov)
#zp = zp.reshape(x.shape)
z.append(zp)
    

fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')

ax.plot_trisurf(points[:,0],points[:,1],z[0], cmap = cm.turbo, antialiased=True)

plt.show()
#%%

#treci_zadatak
#cetvrti zadatak
apo = [1,1]
#%%variance
#a)
mu = [0,3] 
variance = [1.5, 4]

x1 = np.random.normal(mu[0], variance[0], 300)
zeros = np.array([0 for i in range(300)]) 
x1 = np.column_stack((x1,zeros))

x2 = np.random.normal(mu[1], variance[1], 300)
ones = np.array([1 for i in range(300)]) 
x2 = np.column_stack((x2,ones))

def MLRTest(mu,variance,x,apo):
    prob1 = (1/np.sqrt(2*np.pi*variance[0]))*np.exp(-0.5*((x-mu[0])/np.sqrt(variance[0]))**2)
    prob2 = (1/np.sqrt(2*np.pi*variance[1]))*np.exp(-0.5*((x-mu[1])/np.sqrt(variance[1]))**2)
    if (prob1/prob2) > (apo[1]/apo[0]):
        return 0;
    else:
        return 1;

def classifier(x):
    
    s = 0
    for element in x:
        if MLRTest(mu,variance,element[0],apo) == element[1]:
            s+=1
    print(1-(s/len(x)))

classifier(x1)
classifier(x2)

#b)
mu = [-4,3] 
variance = [2, 1.5]

x1 = np.random.normal(mu[0], variance[0], 300)
zeros = np.array([0 for i in range(300)]) 
x1 = np.column_stack((x1,zeros))

x2 = np.random.normal(mu[1], variance[1], 300)
ones = np.array([1 for i in range(300)]) 
x2 = np.column_stack((x2,ones))

def MLRTest(mu,variance,x,apo):
    prob1 = (1/np.sqrt(2*np.pi*variance[0]))*np.exp(-0.5*((x-mu[0])/np.sqrt(variance[0]))**2)
    prob2 = (1/np.sqrt(2*np.pi*variance[1]))*np.exp(-0.5*((x-mu[1])/np.sqrt(variance[1]))**2)
    if (prob1/prob2) > (apo[1]/apo[0]):
        return 0;
    else:
        return 1;

def classifier(x):
    
    s = 0
    for element in x:
        if MLRTest(mu,variance,element[0],apo) == element[1]:
            s+=1
    print(1-(s/len(x)))

classifier(x1)
classifier(x2)

#c)
mu = [2,2] 
variance = [2, 1]

x1 = np.random.normal(mu[0], variance[0], 300)
zeros = np.array([0 for i in range(300)]) 
x1 = np.column_stack((x1,zeros))

x2 = np.random.normal(mu[1], variance[1], 300)
ones = np.array([1 for i in range(300)]) 
x2 = np.column_stack((x2,ones))

def MLRTest(mu,variance,x,apo):
    prob1 = (1/np.sqrt(2*np.pi*variance[0]))*np.exp(-0.5*((x-mu[0])/np.sqrt(variance[0]))**2)
    prob2 = (1/np.sqrt(2*np.pi*variance[1]))*np.exp(-0.5*((x-mu[1])/np.sqrt(variance[1]))**2)
    if (prob1/prob2) > (apo[1]/apo[0]):
        return 0;
    else:
        return 1;

def classifier(x):
    
    s = 0
    for element in x:
        if MLRTest(mu,variance,element[0],apo) == element[1]:
            s+=1
    print(1-(s/len(x)))

classifier(x1)
classifier(x2)
#%%covariance_matrix
#d)
mu = [np.array([3.0, 2.0]),np.array([8.0, 5.0])]
covariance = [[[1.5,0],[0, 1.5]],[[3,0],[0,3]]]

points1=np.random.default_rng().multivariate_normal(mu[0],covariance[0], size = 300)
zeros = np.array([0 for i in range(300)]) 
points1 = np.column_stack((points1,zeros))

points2=np.random.default_rng().multivariate_normal(mu[1],covariance[1], size = 300)
ones = np.array([1 for i in range(300)]) 
points2 = np.column_stack((points2,ones))

def MLRTest(mu,variance,x,apo):
    order = len(mu)
    prob1 = (1/(np.sqrt(2*np.pi)**order)*np.sqrt(np.linalg.det(variance[0])))*np.exp(-0.5*(x-mu[0]).transpose()@np.linalg.inv(variance[0])@(x-mu[0]))
    prob2 = (1/(np.sqrt(2*np.pi)**order)*np.sqrt(np.linalg.det(variance[1])))*np.exp(-0.5*(x-mu[1]).transpose()@np.linalg.inv(variance[1])@(x-mu[1]))

    if (prob1/prob2) > (apo[1]/apo[0]):
        return 0;
    else:
        return 1;

def classifier(x):
    s = 0
    predicts = []
    for element in x:
        predict =MLRTest(mu,covariance,(element[0],element[1]),apo)
        if predict == element[2]:
            s+=1
        predicts.append(predict)
    print(1-(s/len(x)))
    
    return predicts
    
predicts = classifier(points1)
points1 = np.column_stack((points1,predicts))

predicts = classifier(points2)
points2 = np.column_stack((points2,predicts))


plt.scatter(points1[:,0], points1[:,1], c="green")
plt.scatter(points2[:,0], points2[:,1], c="red")
plt.show()

points_categorized = np.concatenate((points1,points2))

points3 = points_categorized[points_categorized[:,3] == 0]
points4 = points_categorized[points_categorized[:,3] == 1]

plt.scatter(points3[:,0], points3[:,1], c="green")
plt.scatter(points4[:,0], points4[:,1], c="red")
plt.show()

#e)
mu = [np.array([3.0, 2.0]),np.array([8.0, 5.0])]
covariance = [[[1,0],[0, 3]],[[2,0],[0,1]]]

points1=np.random.default_rng().multivariate_normal(mu[0],covariance[0], size = 300)
zeros = np.array([0 for i in range(300)]) 
points1 = np.column_stack((points1,zeros))

points2=np.random.default_rng().multivariate_normal(mu[1],covariance[1], size = 300)
ones = np.array([1 for i in range(300)]) 
points2 = np.column_stack((points2,ones))

def MLRTest(mu,variance,x,apo):
    order = len(mu)
    prob1 = (1/(np.sqrt(2*np.pi)**order)*np.sqrt(np.linalg.det(variance[0])))*np.exp(-0.5*(x-mu[0]).transpose()@np.linalg.inv(variance[0])@(x-mu[0]))
    prob2 = (1/(np.sqrt(2*np.pi)**order)*np.sqrt(np.linalg.det(variance[1])))*np.exp(-0.5*(x-mu[1]).transpose()@np.linalg.inv(variance[1])@(x-mu[1]))

    if (prob1/prob2) > (apo[1]/apo[0]):
        return 0;
    else:
        return 1;

def classifier(x):
    s = 0
    predicts = []
    for element in x:
        predict =MLRTest(mu,covariance,(element[0],element[1]),apo)
        if predict == element[2]:
            s+=1
        predicts.append(predict)
    print(1-(s/len(x)))
    
    return predicts
    
predicts = classifier(points1)
points1 = np.column_stack((points1,predicts))

predicts = classifier(points2)
points2 = np.column_stack((points2,predicts))


plt.scatter(points1[:,0], points1[:,1], c="green")
plt.scatter(points2[:,0], points2[:,1], c="red")
plt.show()

points_categorized = np.concatenate((points1,points2))

points3 = points_categorized[points_categorized[:,3] == 0]
points4 = points_categorized[points_categorized[:,3] == 1]

plt.scatter(points3[:,0], points3[:,1], c="green")
plt.scatter(points4[:,0], points4[:,1], c="red")
plt.show()

#f)
mu = [np.array([3.0, 2.0]),np.array([8.0, 5.0])]
covariance = [[[1,0.6],[0.6 , 3]],[[2,-0.6],[-0.6,1]]]

points1=np.random.default_rng().multivariate_normal(mu[0],covariance[0], size = 300)
zeros = np.array([0 for i in range(300)]) 
points1 = np.column_stack((points1,zeros))

points2=np.random.default_rng().multivariate_normal(mu[1],covariance[1], size = 300)
ones = np.array([1 for i in range(300)]) 
points2 = np.column_stack((points2,ones))

def MLRTest(mu,variance,x,apo):
    order = len(mu)
    prob1 = (1/(np.sqrt(2*np.pi)**order)*np.sqrt(np.linalg.det(variance[0])))*np.exp(-0.5*(x-mu[0]).transpose()@np.linalg.inv(variance[0])@(x-mu[0]))
    prob2 = (1/(np.sqrt(2*np.pi)**order)*np.sqrt(np.linalg.det(variance[1])))*np.exp(-0.5*(x-mu[1]).transpose()@np.linalg.inv(variance[1])@(x-mu[1]))

    if (prob1/prob2) > (apo[1]/apo[0]):
        return 0;
    else:
        return 1;

def classifier(x):
    s = 0
    predicts = []
    for element in x:
        predict =MLRTest(mu,covariance,(element[0],element[1]),apo)
        if predict == element[2]:
            s+=1
        predicts.append(predict)
    print(1-(s/len(x)))
    
    return predicts
    
predicts = classifier(points1)
points1 = np.column_stack((points1,predicts))

predicts = classifier(points2)
points2 = np.column_stack((points2,predicts))


plt.scatter(points1[:,0], points1[:,1], c="green")
plt.scatter(points2[:,0], points2[:,1], c="red")
plt.show()

points_categorized = np.concatenate((points1,points2))

points3 = points_categorized[points_categorized[:,3] == 0]
points4 = points_categorized[points_categorized[:,3] == 1]

plt.scatter(points3[:,0], points3[:,1], c="green")
plt.scatter(points4[:,0], points4[:,1], c="red")
plt.show()
#%%

