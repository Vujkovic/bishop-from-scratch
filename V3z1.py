import random
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import math
#prvi_zadatak
def density(x,mu,variance,apo):
    p = [0 for i in range(len(x))]
    for i in range(len(mu)):
        p+=(1/np.sqrt(2*np.pi*variance[i]))*np.exp(-0.5*((x-mu[i])/np.sqrt(variance[i]))**2)*apo[i]
    return p

def sampling(n,line,mu,variance,apo):
    pdf = density(line,mu,variance,apo)
    return np.random.choice(line,n,p=pdf/sum(pdf))

def euclidean_distance(x1, x2):
    return np.sqrt(np.sum((x1-x2)**2))

def knn_estimate(space, sample, k, c):
    
    N = len(sample) 
    pdf = []
    
    for x in space:
        est=0
        distances = np.array([euclidean_distance(s, x) for s in sample])
        sort_idxs = np.argsort(distances)
        R = distances[sort_idxs]
        if R[k] == 0:
            q = k+1
            while(R[q]==0):
                q +=1
            est=k/(N*c*R[q+k])
        else:
            est = k / (N*c*R[k])
        pdf.append(est)

    return pdf
    

#%%Variables a):
mu1 = [0,2.5]
variance1 = [0.2,0.2]
apo1 = [1/3,2/3]
sigma1 = []
for i in variance1:
    sigma1.append(math.sqrt(i))

line1 = np.linspace(-2,5, 1000)

#%%Variables b):
mu2 = [-1,2,4,5,7]
variance2 = [0.2,0.4,0.4,0.1,0.3]
apo2= [0.125,0.25,0.25,0.25,0.125]
sigma2 = []
for i in variance2:
    sigma2.append(math.sqrt(i))

line2 = np.linspace(-3,10, 1000)

#%%
k = 42
c = 2

#%%a)
pdf1 = density(line1, mu1, variance1, apo1)
plt.plot(line1, pdf1, color = 'green')
sample1 = sampling(1000, line1, mu1, variance1, apo1)
est1 = knn_estimate(line1, sample1, k, c)
plt.plot(line1,est1, color='red')
plt.show()

#%%b)
pdf2 = density(line2, mu2, variance2, apo2)
plt.plot(line2, pdf2, color = 'green')
sample2 = sampling(1000, line2, mu2, variance2, apo2)
unique,counts = np.unique(sample2, return_counts=True)
est2 = knn_estimate(line2, sample2, k, c)
plt.plot(line2,est2, color='red')
plt.show()

#%%

