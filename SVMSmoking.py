import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split as tts
from sklearn.metrics import accuracy_score, recall_score, precision_score
from sklearn.utils import shuffle

class SVM():
    
    def __init__(self, regularization_strength, learning_rate):
        self.W = None
        self.learning_rate = learning_rate
        self.regularization_strength = regularization_strength
        
    def fit(self, X,y):
        # train the model
        print("training started...")
        self.W = self.sgd(X.to_numpy(), y.to_numpy())
        print("training finished.")
        print("weights are: {}".format(self.W))
    
    def predict(self,X,y):
        # testing the model
        print("testing the model...")
    
        y_test_predicted = np.array([])
        for i in range(X.shape[0]):
            yp = np.sign(np.dot(X.to_numpy()[i], self.W))
            y_test_predicted = np.append(y_test_predicted, yp)
    
        print("accuracy on test dataset: {}".format(accuracy_score(y, y_test_predicted)))
        print("recall on test dataset: {}".format(recall_score(y, y_test_predicted)))
        print("precision on test dataset: {}".format(recall_score(y, y_test_predicted)))
        

    
    # >> MODEL TRAINING << #
    def compute_cost(self, W, X, Y):
        # calculate hinge loss
        N = X.shape[0]
        distances = 1 - Y * (np.dot(X, W))
        distances[distances < 0] = 0  # equivalent to max(0, distance)
        hinge_loss = regularization_strength * (np.sum(distances) / N)
    
        # calculate cost
        cost = 1 / 2 * np.dot(W, W) + hinge_loss
        return cost
    
    # I haven't tested it but this same function should work for
    # vanilla and mini-batch gradient descent as well
    def calculate_cost_gradient(self, W, X_batch, Y_batch):
        # if only one example is passed (eg. in case of SGD)
        if (type(Y_batch) == np.float64 or type(Y_batch) == np.int64):
            Y_batch = np.array([Y_batch])
            X_batch = np.array([X_batch])  # gives multidimensional array
    
        distance = 1 - (Y_batch * np.dot(X_batch, W))
        dw = np.zeros(len(W))
    
        for ind, d in enumerate(distance):
            if max(0, d) == 0:
                di = W
            else:
                di = W - (regularization_strength * Y_batch[ind] * X_batch[ind])
            dw += di
    
        dw = dw/len(Y_batch)  # average
        return dw
    
    def sgd(self,features, outputs):
        max_epochs = 5000
        weights = np.zeros(features.shape[1])
        nth = 0
        prev_cost = float("inf")
        cost_threshold = 0.01  # in percent
        # stochastic gradient descent
        for epoch in range(1, max_epochs):
            # shuffle to prevent repeating update cycles
            X, Y = shuffle(features, outputs)

            for ind, x in enumerate(X):
                ascent = self.calculate_cost_gradient(weights, x, Y[ind])
                weights = weights - (learning_rate * ascent)
    
            # convergence check on 2^nth epoch
            if epoch == 2 ** nth or epoch == max_epochs - 1:
                cost = self.compute_cost(weights, features, outputs)
                print("Epoch is: {} and Cost is: {}".format(epoch, cost))
                # stoppage criterion
                if abs(prev_cost - cost) < cost_threshold * prev_cost:
                    return weights
                prev_cost = cost
                nth += 1
        return weights

print("reading dataset...")
# read data in pandas (pd) data frame
data = pd.read_csv('./smoking.csv')

# drop last column (extra column added by pd)
# and unnecessary first column (id)
data.drop(data.columns[[0]], axis=1, inplace=True)

data.info()
data.describe()
data.corr()['smoking'].sort_values (ascending=False)
data['oral'].value_counts() #delete
data['tartar'].value_counts()#encode
data['gender'].value_counts()#encode

from sklearn.preprocessing import OneHotEncoder
def oneHot(df,column):
    ohe = OneHotEncoder()
    enc = ohe.fit_transform(df[[column]])
    return enc.toarray()

print("applying feature engineering...")
# put features & outputs in different data frames
diag_map = {'M': -1.0, 'F': 1.0}
data['gender'] = data['gender'].map(diag_map)
diag_map = {'N': -1.0, 'Y': 1.0}
data['tartar'] = data['tartar'].map(diag_map)

Y = data.loc[:, 'smoking']
X = data.iloc[:,:-1]
X=X.drop(columns=['oral'])

# normalize data for better convergence and to prevent overflow
X_normalized = MinMaxScaler().fit_transform(X.values)
X = pd.DataFrame(X_normalized)

# insert 1 in every row for intercept b
X.insert(loc=len(X.columns), column='intercept', value=1)

# split data into train and test set
print("splitting dataset into train and test sets...")
X_train, X_test, y_train, y_test = tts(X, Y, test_size=0.2, random_state=42)

# set hyper-parameters and call init
regularization_strength = 10000
learning_rate = 0.000001
svm = SVM(regularization_strength,learning_rate)
svm.fit(X_train, y_train)
svm.predict(X_test, y_test)