#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 21 21:20:13 2022

@author: milan
"""
import random
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import math
from sklearn.neighbors import KernelDensity
#%%prvi_zadtak
#a)
mu = [0,2.5]
variance = [0.2,0.2]
apo= [1/3,2/3]
sigma = []
for i in variance:
    sigma.append(math.sqrt(i))

line= np.linspace(-2,5, 1000)

def density(x):
    p = [0 for i in range(len(x))]
    for i in range(len(mu)):
        p+=(1/np.sqrt(2*np.pi*variance[i]))*np.exp(-0.5*((x-mu[i])/np.sqrt(variance[i]))**2)*apo[i]
    return p

pdf = density(line)
plt.plot(line, pdf, color='blue')
plt.show()

#b)
mu = [-1,2,4,5,7]
variance = [0.2,0.4,0.4,0.1,0.3]
apo= [0.125,0.25,0.25,0.25,0.125]
sigma = []
for i in variance:
    sigma.append(math.sqrt(i))

line= np.linspace(-3,10, 1000)

def density(x):
    p = [0 for i in range(len(x))]
    for i in range(len(mu)):
        p+=(1/np.sqrt(2*np.pi*variance[i]))*np.exp(-0.5*((x-mu[i])/np.sqrt(variance[i]))**2)*apo[i]
    return p

pdf = density(line)
plt.plot(line, pdf, color='blue')
plt.show()
#%%

#%%drugi_zadatak
#1)
mu = [0,2.5]
variance = [0.2,0.2]
apo= [1/3,2/3]

line= np.linspace(-2,5, 1000)

def density(x,mu,variance):
    p = [0 for i in range(len(x))]
    for i in range(len(mu)):
        p+=(1/np.sqrt(2*np.pi*variance[i]))*np.exp(-0.5*((x-mu[i])/np.sqrt(variance[i]))**2)*apo[i]
    return p

def sampling(n,mu,variance):
    pdf = density(line,mu,variance)
    return np.random.choice(line,n,p=pdf/sum(pdf))


sample = sampling(10000,mu,variance)
#kod bez upotrebe choice funkcije(inverse transform method)
#cdf = np.cumsum(pdf)
#cdf = cdf/cdf[999]
#sample=[]
#for i in range(1000):
#    uniformDraw = np.random.uniform()
#    densityChoice = min(cdf[cdf>=uniformDraw])
#    sampleIndex = np.where(cdf==densityChoice)
#    sample.append(line[sampleIndex][0])

#plt.hist(sample,100)
#plt.show()

#2)
mu = [-1,2,4,5,7]
variance = [0.2,0.4,0.4,0.1,0.3]
apo= [0.125,0.25,0.25,0.25,0.125]

line= np.linspace(-3,10, 1000)

def density(x,mu,variance):
    p = [0 for i in range(len(x))]
    for i in range(len(mu)):
        p+=(1/np.sqrt(2*np.pi*variance[i]))*np.exp(-0.5*((x-mu[i])/np.sqrt(variance[i]))**2)*apo[i]
    return p

def sampling(n,mu,variance):
    pdf = density(line,mu,variance)
    return np.random.choice(line,n,p=pdf/sum(pdf))


sample = sampling(10000,mu,variance)

plt.hist(sample,100)
plt.show()
#%%

#%%treci_zadatak
#a)
def kernel(a):
    if abs(a) < 1/2:
        return 1
    else:
        return 0
    
def KDE(sample, space, h):
    
    pdf = []
    for x in space:
        kernels = 0
        for element in sample:
            kernels += kernel((x-element)/h)
        pdf.append(kernels/(len(sample)*h))
    
    return pdf
#b)c)
sample = sampling(250,mu,variance)
result = KDE(sample,line,0.1)
plt.plot(line,pdf)
plt.plot(line,result)
plt.show()

sample = sampling(500,mu,variance)
result = KDE(sample,line,0.1)
plt.plot(line,pdf)
plt.plot(line,result)
plt.show()

sample = sampling(1000,mu,variance)
result = KDE(sample,line,0.1)
plt.plot(line,pdf)
plt.plot(line,result)
plt.show()

sample = sampling(10000,mu,variance)
result = KDE(sample,line,0.1)
plt.plot(line,pdf)
plt.plot(line,result)
plt.show()
#d) utice tako sto veci broj daje bolju estimaciju funkcije jer je i sam uzorak bolje rasporedjen i lici na raspodelu

#e)
sample = sampling(10000,mu,variance)
result = KDE(sample,line,0.4)
plt.plot(line,pdf)
plt.plot(line,result)
plt.show()
#%%